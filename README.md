
# README #
Cet outil permet de visualiser les liens de filiation entre les différent personnage de l'univers de lineage 2, de façon succinte et simple.

Tout ce passe dans relation.json
## Utilisation ##
Pour pouvoir voir le résultat, vous devez servir les fichiers via un serveur web.  Vous pouvez en installer un très facilement sur votre PC en utilisant XAMPP disponible ici :  https://www.apachefriends.org/fr/index.html

Une foi XAMPP  installé, rendez-vous dans le répertoire d'installation de XAMPP pour trouver le sous-dossier htdocs

créez un sous-dossier perennerelation ou clonez dirrectement le dépos perennerelation dans le dossier htdocs

Démarez le service apache via l'interface de XAMPP

![Alt](./startApacheXampp.png)

accéder via votre navigateur web via http://localhost/perennerelation/diagrame.html


## Identification d'un pnj  ##
Pour identifier un pnj de façon unique et utiliser ses informations dans le schémas, il faut insérer une ligne dans le tableau **nodes** du fichier **relation.json**. A la fin de votre ligne il vous faut rajouter une **,** sauf pour le dernier élément de la liste
Exemple :

    "nodes": [
        {"id": "Einhasad", "group": 1, "size":15},
        {"id": "Gran Kain", "group": 1, "size":15},
        {"id": "Shilen", "group": 2, "size":12}
     ],
*Notez l'élément Shilen qui ne finit pas par une **,***

Pour être valide, un pnj doit avoir les éléments suivants :

  **id**
  : représente le nom du pnj. Ponctuation et espaces autorisés

**groupe**
: représente le groupe auquel appartiens le pnj

**size**
: représente la taille du point qui serra affiché pour représenter le pnj sur le schema


## Identification d'une relation


Pour créer une relation entre deux pnj définit plus haut, il vous faudra l'inserer dans la partie "links" du fichier .json

     {"source": "Einhasad", "target": "Shilen", "value": 1}


**source**
: le nom du premier pnj

**Target**
: le nom du second pnj

**value**
 1. parent/enfant 
 2. subordination
 3. mariage
 4. frère/sœur 

*Les relations suivent la même règle que les pnj en ce qui concerne le formatage des lignes. c.a.d : virgule à la fin de chaque lignes, sauf pour la dernière.*
